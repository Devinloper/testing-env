import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'testing-env';

  formControlItem: FormControl = new FormControl();

  ngOnInit() {
    this.formControlItem.valueChanges.subscribe((value) => console.log(value));
  }
}
